import Vue from "vue";
import App from "./App.vue";
import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";
window.axios = require("axios");

Vue.use(Vuetify, {
  theme: {
    primary: "#00E396", // indigo looks cool #7700cf #69F0AE - old color
    secondary: "#343E57", // old green color "#00E676"
    accent: "#282828"
  }
});

new Vue({
  el: "#app",
  render: h => h(App)
});
